﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StarCooraft_1._0._0
{
    class SCV : Unit
    {
        public SCV(int hp)
        {
            HP = hp;
        }
        // 수리
        public void Repair()
        {
            Console.WriteLine("수리");
        }
        // 공격
        public override int Attack(int damage)
        {
            Console.WriteLine(" SCV 공격");
            return damage;
        }
        // 이동
        public override void Move(int x, int y)
        {
            Console.WriteLine(" SCV 이동");
        }
        // 건설
        public void Construct()
        {
            Console.WriteLine("건설");
        }
        // 채취
        public void Collect()
        {
            Console.WriteLine("채취");
        }

        public override void MakeSound()
        {
            Console.WriteLine("Yes Sir.");
        }

    }


}
