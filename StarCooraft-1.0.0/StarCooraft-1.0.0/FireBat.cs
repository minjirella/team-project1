﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StarCooraft_1._0._0
{
    class FireBat : Unit
    {
        public FireBat()
        {
            HP = 50;
        }
        public override void MakeSound()
        {
            Console.WriteLine("Yes Sir.");
        }

        public override int Attack(int damage)
        {
            return damage;
        }
    }
}
