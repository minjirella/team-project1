﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StarCooraft_1._0._0
{
    abstract class Unit
    {
        protected int HP;
        protected int X;
        protected int Y;

        public virtual void Move(int x, int y)
        {
            if (x < 0 || y < 0)
                return;

            X = x;
            Y = y;

        }


        public abstract int Attack(int damage);
        public abstract void MakeSound();
    }
}
