﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StarCooraft_1._0._0
{
    class CommandCenter : Buildings
    {
        public CommandCenter()
        {
            movable = false;
            HP = 1000;
        }

        public void Lift()
        {
            movable = true;
        }
        public void Land()
        {
            movable = false;
        }

        public override void MakeUnit(int unit_code)
        {
            Console.WriteLine($"unit_code {unit_code}를 생성하였습니다.");
        }

        public override void Move(int x, int y)
        {
            if (movable)
                base.Move(x, y);
        }


    }
}
