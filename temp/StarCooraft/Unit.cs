﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StarGuraft
{
    abstract class Unit
    {
        protected int X;
        protected int Y;

        public virtual void Move(int x, int y)
        {
            if (x < 0 || y < 0)
                return;

            X = x;
            Y = y;
        }

        public abstract void MakeSound();
    }
}
